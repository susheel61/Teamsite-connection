package com.techaspect;

import java.util.Locale;
import java.util.Properties;

import com.interwoven.cssdk.factory.*;
import com.interwoven.cssdk.common.*;
import com.interwoven.cssdk.workflow.CSTask;
import com.interwoven.cssdk.workflow.CSWorkflow;
import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author susheels
 */
public class TeamsiteConnector {

    CSFactory factory = null;
    CSVersion version = null;
    CSClient client = null;

    /**
     * This constructs the factory object.
     *
     * @throws IOException
     * @throws CSException
     */
    public TeamsiteConnector() throws IOException, CSException {
        // read the factory type from a properties file
        InputStream in = getClass().getResourceAsStream("config.properties");
        Properties properties = new Properties();
        properties.load(in);

        // get the factory type we created
        factory = CSFactory.getFactory(properties);
        version = factory.getServerVersion();
        printVersionDetails(version);
        System.out.println("connected to Teamsite!!");
    }

    /**
     * This prints the version of CSSDK Api.
     *
     * @param version
     */
    public static void printVersionDetails(CSVersion version) {
        System.out.println("Running on ContentServices SDK version: " + version.getMajorNumber() + "." + version.getMinorNumber() + "." + version.getPatchNumber());
    }

    /**
     * This takes the username & password and generated CSClient object.
     *
     * @param username username for teamsite connection
     * @param password password for teamsite connection
     * @return This returns CSClient object.
     * @throws CSException
     */
    public CSClient getCSClient(String username, String password) throws CSException {
        // Construct the CSClient object.
        client = factory.getClient(username, "", password, Locale.getDefault(), "Example", null);
        return client;
    }

    /**
     * This take a jobid and creates a CSWorkflow object.
     *
     * @param jobId
     * @return This returns a CSWorkflow object.
     * @throws CSException
     */
    public CSWorkflow getJob(int jobId) throws CSException {
        CSWorkflow workflow = client.getWorkflow(jobId, true);
        return workflow;
    }

    /**
     * This takes a taskID and returns CSTask object.
     *
     * @param taskId Task of the task you want to access in the workflow.
     * @return This returns CSTask object.
     * @throws CSException
     */
    public CSTask getTask(int taskId) throws CSException {
        CSTask task = client.getTask(taskId);
        return task;
    }

}

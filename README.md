<h3>jars Needed</h3>
<strong>We need to get below required jars from the following paths.</strong>
<ul>
  <li>cssdkiface.jar</li>
  <li>cssdkjava.jar</li>
  <li>cssdk_scr.jar</li>
  <li>cssdk_sci.jar</li>
  <li>commons-logging-1.0.4.jar</li>
  <li>commons-pool-1.4.jar</li>
  <li>sci_hopi.jar</li>
  <li>hopi.jar</li>
  <li>teamsite-service.jar</li>
  <li>sharedutils100.jar</li>
  <li>log4j-1.2.15.jar</li>
</ul>

<strong>you can find them at following location</strong>
<ul>
  <li>iw-home\TeamSite\cssdk\java\*</li>
  <li>iw-home\TeamSite\lib\*</li>
  <li>E:\Interwoven\ApplicationContainer\server\default\deploy\iw-cc.war\WEB-INF\lib\log4j-1.2.15.jar</li>
</ul>

<strong>creating a CSClient object:</strong>
```java
TeamsiteConnector conn = new TeamsiteConnector();
CSClient client  = conn.getCSClient("domain\\username", "password");
```
<strong>config.properties has the following information:</strong>
```
com.interwoven.cssdk.factory.CSFactory=com.interwoven.cssdk.factory.CSJavaFactory
defaultTSServer=hostname
ts.server.os=win

